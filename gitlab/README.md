# SNET DEV Team Onboarding - Gitlab
Instructions and best practices on how to use Gitlab among team members.

## Prerequisites
* Having read the [Git's onboarding tutorial](https://code.europa.eu/digit-c4/dev/onboarding/-/tree/main/git)

## SSH Keys
SSH keys are a convenient way to clone or push your repositories without having to enter a username / password each time
```bash
ssh-keygen -t rsa -b 4096
```

Now go to your [Gitlab profile](https://code.europa.eu/-/profile/keys) and [add](https://docs.gitlab.com/ee/user/ssh.html) the newly generated SSH key to your account

## Pull requests
TODO

## CI / CD
To start a CI/CD pipeline, just add a **.gitlab-ci.yml** file in your project's root, commit and push your code. For European Comission projects, this configuration file should contain at least two steps: build and test.

## Running the example
This repository contains a small example project in **onboarding/gitlab/example** that you can look at, copy and experiment on. It shows how to build and test a simple Python backend through a Gitlab pipeline.
