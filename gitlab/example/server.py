#!/bin/python3
# -*- coding: utf-8 -*-
# encoding: utf8
"""
Simple service example to deploy on Gitlab
"""
from http.server import SimpleHTTPRequestHandler
from socketserver import TCPServer
from os import getenv
from base64 import b64decode
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.


USERNAME = getenv("USERNAME", "ENVIRONMENT_ERROR")
PASSWORD = getenv("PASSWORD", "ENVIRONMENT_ERROR")
PORT = getenv("PORT", "ENVIRONMENT_ERROR")


class HTTPHandler(SimpleHTTPRequestHandler):
    """Generic HTTP handler"""
    def do_GET(self):
        """Simple GET endpoint"""
        auth_header = self.headers.get("Authorization")
        authentication_data = auth_header.replace("Basic ", "")
        if not auth_header:
            self.send_response(401)
            self.send_header("WWW-Authenticate", "Basic realm='Test API'")
            self.end_headers()
            return

        username, password = [str(data, "utf-8") for data in b64decode(authentication_data).split(b":")]
        if username != USERNAME or password != PASSWORD:
            self.send_response(401)
            self.send_header("WWW-Authenticate", "Basic realm='Test API'")
            self.end_headers()
            return

        self.send_response(200)
        self.send_header("Content-type", "text/plain")
        self.end_headers()
        self.wfile.write(b"Hello, world!")


if __name__ == "__main__":
    print(f"Starting server on port {PORT}")
    server = TCPServer(("0.0.0.0", PORT), HTTPHandler)
    try:
        server.serve_forever()
    except KeyboardInterrupt:
        print("Bye!")
        server.shutdown()
