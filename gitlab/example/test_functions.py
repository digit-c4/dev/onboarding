#!/bin/python3
# -*- coding: utf-8 -*-
# encoding: utf8
"""
Simple test suite example
"""
import unittest
from os import getenv
from base64 import b64encode
from requests import get as requests_get
from dotenv import load_dotenv

load_dotenv()  # take environment variables from .env.

USERNAME = getenv("USERNAME", "ENVIRONMENT_ERROR")
PASSWORD = getenv("PASSWORD", "ENVIRONMENT_ERROR")
URL = getenv("URL", "ENVIRONMENT_ERROR")


class Testfunctions(unittest.TestCase):
    """Simple test suite"""

    def test_connection(self):
        """Test the connection to the API"""
        auth_header = str(b64encode(f"{USERNAME}:{PASSWORD}".encode()), "utf-8")
        response = requests_get(URL, headers={"Authorization": f"Basic {auth_header}"}, timeout=10)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.content, b"Hello, world!")


if __name__ =="__main__":
    unittest.main()
