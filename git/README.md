# SNET DEV Team Onboarding - Git
Everything to know about SNET's Git environment.

| :warning: WARNING          |
|:---------------------------|
| This documentation will postulate that the reader already have some basic knowledge on how to use Git (see the "prerequisites" section for learning resources suggestions) |

## Prerequisites
* Having read the 3 first chapters of the Pro Git [book](https://git-scm.com/book/en/v2) or having a similar experience in Git

## Development environment setup

### Global config
This is the place that will contain your global config parameters. To get your Redmine API key, go to your Redmine account page and copy the value under **My account > API access key**.

```bash
nano ~/.gitconfig
```

Put this in your global configuration file:
```ini
[http]
    proxy = proxy-address
[user]
    name = Firstname LASTNAME
    email = firstname.lastname@email.com
[core]
    gitproxy = git-proxy
    editor = nano
[socks]
    proxy = proxy-address
[redmine]
    apikey = your-redmine-api-key
```

## Day to day usage
The standard workflow of the team is to start working on something by creating an issue on Redmine to describe a bug, an improvement or a new feature, and then to commit everything related to that issue with the issue number contained in the commit message.

Eg. ```7a977aa - (origin/master, origin/HEAD) refs #32144 fixed locations issues for wing-orphaned plugs (3 weeks ago) <Firstname Lastname>```

### Opening issues on Redmine
Go on Redmine, click on the relevant project and then on **Issues > new Issue**. Copy the issue number, you will need it later on.

### Branching strategies
Everytime an issue is created, a branch should also be created when someone starts to work on that issue. This helps to track the different changes in the repositories and reduce the complexity when multiple people are working on the same repository.

### Branch naming
Syntax: type-snetusername-redmineissuenumber-featurename

**Example:**
* Type: feature / bug / improvement
* Snet username: your-snet-username
* Redmine issue number: 29351
* Feature name: automatic-emails-on-form-validation
* Result: feature-your-snet-username-29351-automatic-emails-on-form-validation

### Pushing repo on different environments
Moving code between environments (dev, acceptance and production) is done using Ansible playbooks.

#### Moving the code on acceptance
```bash
sudo -u snet ansible-playbook-2.9 playbook-deploy-repo.yml --extra-vars "target=target-server target_repo=['your_repo']" -u ansible --private-key "/opt/ansible/.ssh/id_rsa"
```

#### Moving the code on all prod nodes
```bash
sudo -u snet ansible-playbook-2.9 playbook-debug-repo.yml -i /opt/ansible/hosts --extra-vars "target=vworker_buster_prd target_repo=['your_repo']" -u ansible --private-key "/opt/ansible/.ssh/id_rsa"
```
