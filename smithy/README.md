# Amazon Smithy - Onboarding Guide
This guide provide assistance for newcomers in installing and using the Amazon Smithy tools and generators. For more informations about the Smithy Interface Definition Language and models, please visit [the official Smithy documentation website](https://smithy.io/2.0/index.html)

For more examples, please have a look at the official [Smithy examples repository](https://github.com/smithy-lang/smithy-examples) on Github.

| :warning: WARNING          |
|:---------------------------|
| This tutorial shows how to install Smithy on Linux and Windows but the only way it worked in the EUC environment was by using a Debian-based lab VM |

## Pre-requisites
The only pre-requisite is the JDK (Java Development Kit). To install it, use one of the following procedures depending on your operating system.

### Linux
```sh
sudo apt install default-jre default-jdk -y
```

### Windows
* Open the start menu
* Enter **Software Center** in the search bar, click on the first result
* In the Software Center's search bar, search for **jdk**
* Click on **Java SE Development Kit**
* Click on the **Install** button

## Download this repository
```sh
# Prepare project structure
mkdir -p $HOME/Workspace/code.europa.eu/digit-c4/dev
cd $HOME/Workspace/code.europa.eu/digit-c4/dev
git clone https://code.europa.eu/digit-c4/dev/onboarding.git
```

## Smithy CLI Installation
| :warning: WARNING          |
|:---------------------------|
| Currently we only use gradle instead of the Smithy CLI on our lab machines because Smithy does not provide a way for proxy configuration |

### Linux
```sh
# Download
mkdir -p smithy-install/smithy && curl -L https://github.com/smithy-lang/smithy/releases/download/1.41.1/smithy-cli-linux-x86_64.tar.gz -o smithy-install/smithy-cli-linux-x86_64.tar.gz && tar -xvzf smithy-install/smithy-cli-linux-x86_64.tar.gz -C smithy-install/smithy

# Install
sudo smithy-install/smithy/install
```

### Windows
* Go on the [Smithy Github releases webpage]()
* Localize the latest version (**1.41.1** at the time of writing this guide)
* In the **Assets** section, click on the button to **Show all assets**
* Download the **smithy-cli-windows-x64.tar.gz** archive
* ```cd ~/Downloads```
* ```mkdir smithy-install```
* Extract it somewhere ```tar -xvzf .\smithy-cli-windows-x64.tar.gz -C smithy-install```
* Use the install script ```cd smithy-install && ./install.bat```

## Gradle Installation
Please refer to the [official Gradle Website](https://gradle.org/install/#manually). A manual installation is currently the recommended way to go if you want to avoid disk space issues that would arise with SDK or Homebrew.

## Usage
In the following example, we demonstrate how to generate an OpenAPI spec from the Smithy's quick-start 'Weather Forecast' example
```sh
cd example
cp gradle.properties.example gradle.properties
# Fill in gradle.properties with the correct proxy settings so Gradle will be able to fetch sources on Maven
gradle build
# Once this is done, a good practice is to copy the openapi.json output in your-project-root/doc/api/openapi/openapi.json
# so people browsing your repository will be able to have a quick overview of the API routes your service exposes
mkdir -p doc/api/openapi
cp build/smithyprojections/example/source/openapi/Weather.openapi.json doc/api/openapi/openapi.json
```
