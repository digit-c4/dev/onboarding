#!/usr/bin/env bash
DEBUG=false

# Check if .optic.env file exists
if test -f .optic.env; then
    echo "Loading .optic.env file..."
    export $(cat .optic.env | xargs)
else
    echo "No .optic.env file found"
    exit 1
fi

# Test if all variables are set
if [ -z "$AUTHORIZATION" ] && [ -z "$OPTIC_PROXY" ] && [ -z "$AUTH_USER" ] && [ -z "$AUTH_GROUP" ] && [ -z "$BASE_URL" ]; then
    echo "One or more variables are undefined. Have a look at the .env.example file to see which variables are required."
    exit 1
fi

# Change TEST_ENDPOINT depending of DEBUG status
if $DEBUG; then
    echo "DEBUG = $DEBUG"
    TEST_ENDPOINT="$BASE_URL/snet/api/DAY0/test/"
else
    TEST_ENDPOINT="$OPTIC_PROXY/test/"
fi

# Start queries
echo "Start querying $TEST_ENDPOINT"
curl --noproxy "*" -kvv $TEST_ENDPOINT \
    -H "Authorization: Basic $AUTHORIZATION" \
    -H "AuthUser: $AUTH_USER" \
    -H "AuthGroup: $AUTH_GROUP"

echo "Done!"
