# Optic - Onboarding Guide
In this lab, we'll explore Optic, a powerful tool for automating and maintaining API documentation. Optic utilizes OpenAPI, a standardized API description format, to generate comprehensive and up-to-date documentation for your internal APIs.

Through this hands-on lab, you'll gain hands-on experience in using Optic to:
1. Configure Optic to monitor your API endpoints
2. Automate OpenAPI documentation generation
3. Inspect and review generated API documentation
4. Leverage Optic for API change tracking and notification

## Installation
```sh
# Prepare project structure if necessary
mkdir -p $HOME/Workspace/code.europa.eu/digit-c4/dev
cd $HOME/Workspace/code.europa.eu/digit-c4/dev

# Clone repository
git clone https://code.europa.eu/digit-c4/dev/onboarding.git
```

## Usage
Optic can be used in different ways. I successfully tested it using [curl](https://www.useoptic.com/docs/capturing-traffic/examples#curl) and [HAR files](https://www.useoptic.com/docs/capturing-traffic/har-postman#har-files), but there are [other ways](https://www.useoptic.com/docs/capturing-traffic/examples)

### 1. Using HAR files
The simplest way to capture traffic using Optic is by feeding it ```.har``` files. To do so, open your favorite chrome-based browser (I use Edge), go on the SNET API description's page (eg. BASE_URL/snet/api/YOUR-PROJECT/index.html) and open the inspection tool. Then, use the **Try it!** button under a couple of requests and go see the results in the **Network** tab of the dev tools. Select the last call of the list, right-click on it and choose the **Save all as HAR with content** option.
![Screenshot - how to download a .har file from the dev tools](../images/optic-har-a.png "How to download a .har file from the dev tools")

Once this is done, upload it in ```$HOME/Workspace/code.europa.eu/digit-c4/dev/onboarding/optic``` and issue the following command:
```sh
optic capture openapi.yml --har requests.har --update interactive --verbose
Initializing OpenAPI file at openapi.yml

Learning path patterns for unmatched requests...
> /snet/{snet}/DAY0/{DAY0}/
✔ Is this the right pattern for GET /snet/api/DAY0/test/ › no
✔ Provide the correct path (e.g. /api/users/{userId}) … /snet/api/DAY0/test/
> /snet/api/DAY0/stack/
✔ Is this the right pattern for GET /snet/api/DAY0/stack/ › yes
Documenting new operations:
✔ GET /snet/api/DAY0/stack/
✔ GET /snet/api/DAY0/test/
```
If you look at the output of the previous command, you'll notice that it didn't provided a correct path by itself for the /test/ endpoint. I had to correct it manually and then it succeeded itself with the second endpoint /stack/

### 2. Using a Curl script
Using a Curl script is a harder (you will have to write every single call yourself in **requests.sh**) but can be useful to debug why an endpoint returns an unexpected HTTP code because we can leverage the Curl debug features (the -v, -vv or -vvv flags).

To see it in action, make sure the second capture request example in ```optic/optic.yml``` is uncommented. Like in this example:
```yaml
capture:
  openapi.yml:
    server:
      # Base URL of the service you're testing
      url: '{{BASE_URL}}/snet/api/DAY0'

    # This other example shows how to use Optic in conjuctions with a curl script to make requests
    requests:
      run:
        command: bash ./requests.sh
```

Now use the following command to start it:
```sh
optic capture openapi.yml --update interactive --verbose
```

As you notice, the output is much (MUCH) more verbose that the one you get using .har files.
