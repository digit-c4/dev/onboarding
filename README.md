# Dev Team - Onboarding Procedure
As a new member of the dev team here at the European Commission, you will need to add a few essential techs to your skill set, and this repository is here to help you to achieve that goal.

At any point, if you feel that something is wrong, not clear or subject to improvements, please [open a new issue](https://code.europa.eu/digit-c4/dev/onboarding/-/issues/new).

## Pre-requisites
Your lab VM must have been initialized first using [this procedure](https://code.europa.eu/digit-c4/dev/ansible-collection/-/tree/main/example?ref_type=heads#dev-vm-initialization).

## Recommended order of completion
1. [Git](https://code.europa.eu/digit-c4/dev/onboarding/-/tree/main/git?ref_type=heads)
2. [GitLab](https://code.europa.eu/digit-c4/dev/onboarding/-/tree/main/gitlab?ref_type=heads)
3. [Netbox](https://code.europa.eu/digit-c4/dev/netbox-training-workspace)
4. [Smithy](https://code.europa.eu/digit-c4/dev/onboarding/-/tree/main/smithy?ref_type=heads)
5. [Optic](https://code.europa.eu/digit-c4/dev/onboarding/-/tree/main/optic?ref_type=heads)
